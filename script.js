const numberInput = document.querySelector('#number')
const convertBtn = document.querySelector('#convert-btn')
const result = document.querySelector('#output')
const romanNumerals = {
  M: 1000,
  CM: 900,
  D: 500,
  CD: 400,
  C: 100,
  XC: 90,
  L: 50,
  XL: 40,
  X: 10,
  IX: 9,
  V: 5,
  IV: 4,
  I: 1,
};

const convertNumber = (input) => {
  let result = ''

  for (const key in romanNumerals) {
    while(input >= romanNumerals[key]) {
      result += key
      input -= romanNumerals[key]
    }
  }

  return result
}
const checkUserInput = () => {
  const inputInt = parseInt(numberInput.value)

  if (!numberInput.value) {
    result.innerText = "Please enter a valid number"
    return
  } else if (inputInt <= 1) {
    result.innerText = "Please enter a number greater than or equal to 1"
    return
  } else if (inputInt > 3999) {
    result.innerText = "Please enter a number less than or equal to 3999"
    return
  }

  result.innerText = convertNumber(inputInt)
}

convertBtn.addEventListener('click', checkUserInput)